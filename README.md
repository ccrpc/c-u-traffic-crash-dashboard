# Champaign County Traffic Crash Dashboard

[Champaign County Traffic Crash Dashboard](https://maps.ccrpc.org/crash-dashboard/) is an interactive web application that presents traffic crash statistics and maps for communities in Champaign County, IL. 

![Screenshot](C-U_Traffic_Crash_Dashboard_updated_risk_map_temp.JPG)

## Getting Started

### Prerequisites
*  >  In your terminal, install several Dash libraries. These libraries are under active development, so install and upgrade frequently. Python 2 and 3 are supported.   
*     `pip install dash==1.19.0`  
*     `pip install plotly`  
*     `pip install pandas`  
*     `pip install numpy`  
*  [Mapbox](https://www.mapbox.com/) token  
*  Traffic crash data  

### Run the app
Run the app with
`$ python crash_dash.py` (Press CTRL+C to quit)
and visit http://127.0.0.1:8050/ in your web browser.

### Deploy the app
Learn about deploying the Dash app [here](https://dash.plotly.com/deployment)

## Author
The C-U Traffic Crash Dashboard was developed on [Plotly.js Dash framework](https://plot.ly/dash/) by Shuake Wuzhati for the Champaign County Regional Planning Commission.

## License
This project is licensed under BSD 3-Clause License - see the LICENSE.md file for details.

