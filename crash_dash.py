##########################################################################
## Champaign County Traffic Crash Dashboard
##########################################################################
## Built on Plotly.js Dash framework
##########################################################################
## Author: Shuake Wuzhati
## Copyright: Champaign County Regional Planning Commission {2021}
## Version: 9.0
##    -- update crash data to 2017-2021
## Maintainer: Shuake Wuzhati
## Email: swuzhati@ccrpc.org
###########################################################################

# Import libraries
import os
import ast
import urllib

import pandas as pd
import plotly.express as px
import plotly.graph_objects as go

import dash
import dash_core_components as dcc
import dash_html_components as html
import dash_table
from dash.dependencies import Input, Output

import geopandas as gpd
import shapely.geometry
from shapely import wkt
import numpy as np
import json

# Dash app initialization
URL_BASE_PATHNAME = os.getenv('URL_BASE_PATHNAME', '/')
app = dash.Dash(__name__, url_base_pathname=URL_BASE_PATHNAME)
app.title = 'Champaign County Traffic Crash Dashboard'

# app deployment information can be found https://dash.plot.ly/deployment

end_yr = 2023
start_yr = end_yr - 4
crash_layer = str(start_yr) +'-' + str(end_yr) + 'crash data'
years_range = list(range(start_yr, end_yr + 1))[::-1]

# Import data from L:\Safety Forecasting Tool\
df = pd.read_csv("data/crashes_"+str(start_yr) +'_' + str(end_yr)+".csv")
df_prediction = pd.read_csv(os.getenv("CSV_FILE_RISK_PREDICTION", "data/dashboard_risk_prediction.csv"))

df_prediction_dropna=df_prediction.copy().dropna()
df.loc[df.Cause1.isnull(), 'Cause1'] = 'Null'

# Add mapbox access token here
mapbox_access_token = os.getenv("MAPBOX_KEY", "NO_TOKEN")
if mapbox_access_token == "NO_TOKEN":
    raise ValueError("MAPBOX_KEY should be set in the environment variable")

def query_and_serialize_data(selected_Geography, selected_Year, session_id):
    dff=df[df['CrashYear'] == selected_Year]
    if selected_Geography=='ChampaignCounty':
        return dff
    elif selected_Geography=='Urban':
        return dff[dff['MPA'].values == True]
    elif selected_Geography=='Rural':
        return dff[dff['MPA'].values == False]
    elif selected_Geography=='University':
        return dff[dff['University'].values == True]
    else:
        return dff[dff['CityName'].values == selected_Geography]

# Function to cache filtered dataframe by user sessions
def get_dataframe(selected_Geography, selected_Year,session_id):
    """Cache filtered dataframe by user sessions

    Args:
        selected_Geography (str): Chamapign County, University District, municipalities, Urban (Within MPA boundary), Rural (Outside of MPA boundary)
        selected_Year (int): start_yr - end_yr
        session_id: user session

    Returns:
        pandas dataframe: cached dataframe
        """
    return query_and_serialize_data(selected_Geography, selected_Year, session_id)

# Function to generate the Crash Map
def gen_map(map_data):
    """Generate crash map

    Args:
        map_data: pandas dataframe with longitude and latitude of crashes

    Returns:
        Plotly figure"""

    px.set_mapbox_access_token(mapbox_access_token)
    figure = px.scatter_mapbox(
        map_data,
        lat="TSCrashLat",
        lon="TSCrashLong",
        color="CrashInjuriesSeverity",
        color_discrete_map={
            "Fatal Crash": "red",
            "A Injury Crash": "#fcba03",
            "B Injury Crash": "#824904",
            "C Injury Crash": "#1f77b4",
            "No Injuries": "#a07eba"
        },
        category_orders={
            "CrashInjuriesSeverity": [
                "No Injuries",
                "C Injury Crash",
                "B Injury Crash",
                "A Injury Crash",
                "Fatal Crash",
            ]
        },
        center=dict(lat=40.112594, lon=-88.243545),
        zoom=10,
        height=350,
        mapbox_style="basic",
        hover_data=[
            "CrashYear",
            "CityName",
            "TotalFatals",
            "TotalInjured",
            "TypeofFirstCrash",
            "Cause1",
            "WeatherCondition",
            "LightCondition",
            "RoadSurfaceCond",
            'heavy'
        ],
        labels=dict(
            CrashYear="Crash year",
            CityName="Municipality",
            CrashInjuriesSeverity="Crash severity",
            TotalFatals="Total number of fatalities",
            TotalInjured="Total number of injuries",
            TypeofFirstCrash="Crash type",
            Cause1="Main crash cause",
            WeatherCondition="Weather condition",
            LightCondition="Light condition",
            RoadSurfaceCond="Road surface condition",
            heavy='Heavy vehicles invovled crash',
            TSCrashLat="Latitude",
            TSCrashLong="Longitude",
        ),
    )
    figure.update_layout(
        margin=dict(l=0, r=0, t=0, b=0),
        font=dict(
            family="Arial Black"
        ),
        legend_title="<b> Crash Severity </b>"
    )

    return figure

# Function to generate the Crash Risk Map
def gen_prediction_map(df_prediction):
    fig = px.line_mapbox(
        df_prediction,
        lat="lats", 
        lon="lons", 
        hover_name="risk_levels",
        zoom=10,
        center=dict(lat=40.112594, lon=-88.243545),
        height=350,
        #color="risk_levels",
        mapbox_style="basic",)
    fig.update_layout(margin=dict(l=0, r=0, t=0, b=0))
    return fig

def gen_prediction_point_map(df_prediction_dropna):
    px.set_mapbox_access_token(mapbox_access_token)
    fig = px.scatter_mapbox(
        df_prediction_dropna,
        lat="lats", 
        lon="lons", 
        # hover_name="risk_levels",
        zoom=10,
        center=dict(lat=40.112594, lon=-88.243545),
        height=350,
        color="risk_levels",
        color_discrete_map={
            "Low Risk": "#a07eba",
            "Medium Risk": "#fcba03",
            "High Risk": "red"},
        mapbox_style="basic",
        labels=dict(
            lats="Latitude",
            lons="Longitude",
            risk_levels="Risk Levels"
        ),
        )
    fig.update_layout(
        margin=dict(l=0, r=0, t=0, b=0),
        font=dict(
            family="Arial Black"
            ),
        legend_title="<b> Risk Levels </b>")
    return fig

def gen_risk_map(map_data):
    """Generate crash density map

    Args:
        map_data: pandas dataframe with longitude and latitude of crashes

    Returns:
        Plotly figure"""

    px.set_mapbox_access_token(mapbox_access_token)
    figure = px.density_mapbox(
        map_data,
        lat="TSCrashLat",
        lon="TSCrashLong",
        radius=5,
        center=dict(lat=40.112594, lon=-88.243545),
        zoom=10,
        height=350,
        mapbox_style="basic",
    )
    figure.update_layout(margin=dict(l=0, r=0, t=0, b=0))
    return figure

# Web app layout
app.layout = html.Div(  # Entire webpage
    html.Div(  # Entire container
        [
            html.Div(  # Title & logo container
                [
                    html.Div(  # Title container
                        [
                            html.A(
                                html.H1(
                                    title="Home",
                                    children="Champaign County Traffic Crash Dashboard",
                                    className="nine columns",
                                    style={
                                        "margin-left": 12,
                                        "color": "#1F77B4",
                                        "float": "left"
                                    },
                                ),
                                href=URL_BASE_PATHNAME,
                            )
                        ],
                    ),
                    html.Div(  # Logo container
                        [
                            html.A(
                                html.Img(
                                    title="Logo",
                                    src="https://cms3.revize.com/revize/champaigncountyrpc/revize_photo_gallery/About%20RPC/rpc_logo_horizontal.png",
                                    className="three columns",
                                    style={
                                        "height": "10%",
                                        "width": "10%",
                                        "float": "right",
                                        "position": "relative",
                                        "padding-top": 0,
                                        "padding-right": 0,
                                        "margin-right": 20,
                                    },
                                ),
                                href="https://ccrpc.org/",
                                target="_blank"
                            )
                        ],
                        style={"margin-top": -20,}
                    ),
                ],
                className="row",
            ),
            html.Div(  # Graphics container
                [
                    html.Div(  # Statistics container
                        [
                            html.Div(
                                     id='session-id',
                                     style={'display': 'none'}
                                     ),
                            html.Div(# Total number of crashes container
                                     [
                                         html.Button(
                                             [
                                                 html.H6("Crashes"),
                                                 html.H5(id="TotalCrashText")
                                             ],
                                             id='total_crash_click',
                                             n_clicks=0
                                             )
                                    ],
                                id="TotalCrash",
                                className="bare_container one columns",
                                style={'height': 80}
                                ),
                            html.Div(
                                dash_table.DataTable(
                                    columns=[
                                        {
                                            "name": "Geography",
                                            "id": "Geography",
                                        },
                                        {"name": "Year", "id": "Year"},
                                        {
                                            "name": "Total number of crashes",
                                            "id": "Total number of crashes",
                                        },
                                    ],
                                    id="TotalCrashTable",
                                ),
                                className="hidden",
                            ),
                            html.Div( # Total number of fatalities container
                                     [
                                         html.Button(
                                             [
                                                 html.H6("Fatalities"),
                                                 html.H5(id="FatalitiesText")
                                             ],
                                             id='fatality_click',
                                             n_clicks=0
                                             )
                                    ],
                                id="TotalFatalities",
                                className="bare_container one columns",
                                style={'height': 80},
                                title='Total Number of Fatalities'
                                ),
                            html.Div(
                                dash_table.DataTable(
                                    columns=[
                                        {
                                            "name": "Geography",
                                            "id": "Geography",
                                        },
                                        {"name": "Year", "id": "Year"},
                                        {
                                            "name": "Total number of fatalities",
                                            "id": "Total number of fatalities",
                                        },
                                    ],
                                    id="FatalitiesTable",
                                ),
                                className="hidden",
                            ),
                            html.Div( # Total number of severe injuries container
                                [
                                    html.Button(
                                             [
                                                 html.H6("Severe Injuries"),
                                                 html.H5(id="SevereInjuriesText")
                                             ],
                                             id='injury_click',
                                             n_clicks=0
                                             )
                                    ],
                                id="SevereInjuries",
                                className="bare_container one columns",
                                style={'height': 80},
                                title='Total Number of Severe Injuries'
                                ),
                            html.Div(
                                dash_table.DataTable(
                                    columns=[
                                        {
                                            "name": "Geography",
                                            "id": "Geography",
                                        },
                                        {"name": "Year", "id": "Year"},
                                        {
                                            "name": "Total number of severe injuries",
                                            "id": "Total number of severe injuries",
                                        },
                                    ],
                                    id="SevereInjuriesTable",
                                ),
                                className="hidden"
                            ),
                            html.Div( # Total number of bicycle crashes container
                                [
                                    html.Button(
                                             [
                                                 html.H6("Bicycle Crashes"),
                                                 html.H5(id="BikeCrashesText")
                                             ],
                                             id='bike_click',
                                             n_clicks=0
                                             )
                                    ],
                                id="BikeCrashes",
                                className="bare_container one columns",
                                style={'height': 80},
                                title='Total Number of Bicycle Crashes'
                                ),
                            html.Div(
                                dash_table.DataTable(
                                    columns=[
                                        {
                                            "name": "Geography",
                                            "id": "Geography",
                                        },
                                        {"name": "Year", "id": "Year"},
                                        {
                                            "name": "Total number of bicycle crashes",
                                            "id": "Total number of bicycle crashes",
                                        },
                                    ],
                                    id="BikeCrashesTable",
                                ),
                                className="hidden",
                            ),
                            html.Div( # Total number of pedestrian crashes container
                                [
                                    html.Button(
                                             [
                                                 html.H6("Pedestrian Crashes"),
                                                 html.H5(id="PedCrashesText")
                                             ],
                                             id='ped_click',
                                             n_clicks=0
                                             )
                                    ],
                                id="PedCrashes",
                                className="bare_container one columns",
                                style={'height': 80},
                                title='Total Number of Pedestrian Crashes'
                                ),
                            html.Div(
                                    dash_table.DataTable(
                                                         columns=[
                                                                  {
                                                                   'name': 'Geography',
                                                                   'id': 'Geography'
                                                                   },
                                                                  {'name': 'Year', 'id': 'Year'},
                                                                  {
                                                                   'name': 'Total number of pedestrian crashes',
                                                                   'id': 'Total number of pedestrian crashes'
                                                                   }
                                                                  ],
                                                         id='PedCrashesTable'
                                                         ),
                                    className='hidden'
                                    ),
                            html.Div( # Total number of heavy vehicle crashes container
                                [
                                    html.Button(
                                             [
                                                 html.H6("Heavy Vehicle Crashes"),
                                                 html.H5(id="HeavyCrashesText")
                                             ],
                                             id='heavy_click',
                                             n_clicks=0
                                             )
                                    ],
                                id="HeavyCrashes",
                                className="bare_container one columns",
                                style={'height': 80},
                                title='Total Number of Heavy Vehicle Crashes'
                                ),
                            html.Div(
                                    dash_table.DataTable(
                                                         columns=[
                                                                  {
                                                                   'name': 'Geography',
                                                                   'id': 'Geography'
                                                                   },
                                                                  {'name': 'Year', 'id': 'Year'},
                                                                  {
                                                                   'name': 'Total number of heavy vehicle crashes',
                                                                   'id': 'Total number of heavy vehicle crashes'
                                                                   }
                                                                  ],
                                                         id='HeavyCrashesTable'
                                                         ),
                                    className='hidden'
                                    ),
                            html.Div( # Link to Performance Measures
                                [
                                    html.A(
                                        html.H3("C-U Safety Plans Performance Measures"),
                                        href='https://ccrpc.org/search.php?q=safety+plan+report+card+"vision+zero"',
                                        target='_blank'
                                    )
                                    ],
                                id="SafetyPM",
                                className="bare_container one columns",
                                style={'height': 80},
                                title='Safety Performance Measures'
                                ),
                            html.Div(  # Geography and year filter container
                                [
                                    html.H6(
                                        "Select geography and year"
                                    ),
                                    dcc.Dropdown(
                                        id="Geography",
                                        options=[
                                            {
                                                "label": "Champaign County",
                                                "value": "ChampaignCounty",
                                            },
                                            {
                                                "label": "City of Champaign",
                                                "value": "Champaign",
                                            },
                                            {
                                                "label": "City of Urbana",
                                                "value": "Urbana",
                                            },
                                            {
                                                "label": "Village of Savoy",
                                                "value": "Savoy",
                                            },
                                            {
                                                "label": "Village of Mahomet",
                                                "value": "Mahomet",
                                            },
                                            {
                                                "label": "Village of Rantoul",
                                                "value": "Rantoul",
                                            },
                                            {
                                                "label": "University District",
                                                "value": "University",
                                            },
                                            {
                                                "label": "Urban (MPA)",
                                                "value": "Urban",
                                            },
                                            {
                                                "label": "Rural (outside of MPA)",
                                                "value": "Rural",
                                            },
                                        ],
                                        value="ChampaignCounty",
                                        className="fourhalf columns",
                                    ),
                                    dcc.Dropdown(
                                        id="Year",
                                        options = [
                                            {'label': str(year), 'value': year % 100} for year in years_range
                                            ],
                                        value=end_yr % 100,
                                        className="fourhalf columns",
                                        style={"margin-left": 10},
                                    ),
                                    html.Div(
                                        [
                                            html.A(
                                                html.Button(
                                                    "Refresh",
                                                    style={
                                                        "color": "white",
                                                        "border": "2px solid",
                                                        #'width': '80%'
                                                    },
                                                ),
                                                href=URL_BASE_PATHNAME,
                                            )
                                        ],
                                        className="twohalf columns",
                                        style={"margin-left": 20},
                                    ),
                                ],
                                id="FilterGeography",
                                className="bare_container four columns",
                                style={"height": 80},
                                title="Select Geography and Year and Refresh Page",
                            ),
                        ],
                        className="row",
                    ),
                    html.Div(  # Charts container
                        [
                            html.Div(  # Crash types and Monthly distribution container
                                [
                                    html.Div(  # Crash types container
                                        [
                                            html.Div(
                                                [
                                                    html.H6(
                                                        children="Top Crash Types"
                                                    ),
                                                    html.A(
                                                        'Download Data',
                                                        id='type_download',
                                                        download='crash_type.csv',
                                                        href="",
                                                        target="_blank",
                                                        style={"float": "right",}
                                                        )
                                                    ]
                                                ),
                                            dcc.Graph(id="CrashTypeGraph"),
                                        ],
                                        className="bare_container seven columns",
                                        title="Top Crash Types",
                                    ),
                                    html.Div(
                                        dash_table.DataTable(
                                            columns=[
                                                {
                                                    "name": "Geography",
                                                    "id": "Geography",
                                                },
                                                {"name": "Year", "id": "Year"},
                                            ],
                                            id="CrashTypeTable_1",
                                        ),
                                        className="hidden",
                                    ),
                                    html.Div(
                                        dash_table.DataTable(
                                            columns=[
                                                {
                                                    "name": "Top Crash Types",
                                                    "id": "Crash Type",
                                                },
                                                {
                                                    "name": "Number of Crashes",
                                                    "id": "Type Number of Crashes",
                                                },
                                            ],
                                            id="CrashTypeTable_2",
                                        ),
                                        className="hidden",
                                    ),
                                    html.Div( # Monthly distribution container
                                        [
                                            html.Div(
                                                [
                                                    html.H6(
                                                        children="Monthly Distribution"
                                                    ),
                                                    html.A(
                                                        'Download Data',
                                                        id='monthly_download',
                                                        download='monthly.csv',
                                                        href="",
                                                        target="_blank",
                                                        style={"float": "right",}
                                                        )
                                                ]
                                                ),
                                            dcc.Graph(id="MonthlyGraph"),
                                        ],
                                        className="bare_container five columns",
                                        title="Monthly Distribution",
                                    ),
                                    html.Div(
                                        dash_table.DataTable(
                                            columns=[
                                                {
                                                    "name": "Geography",
                                                    "id": "Geography",
                                                },
                                                {"name": "Year", "id": "Year"},
                                                {
                                                    "name": "Crash Type",
                                                    "id": "Selected Crash Type",
                                                },
                                            ],
                                            id="MonthlyTable_1",
                                        ),
                                        className="hidden",
                                    ),
                                    html.Div(
                                        dash_table.DataTable(
                                            columns=[
                                                {
                                                    "name": "Month",
                                                    "id": "Month",
                                                },
                                                {
                                                    "name": "Number of Crashes",
                                                    "id": "Monthly Number of Crashes",
                                                },
                                            ],
                                            id="MonthlyTable_2",
                                        ),
                                        className="hidden",
                                    ),
                                ],
                                className="row",
                            ),
                            html.Div(  # Crash cause and Daily distribution container
                                [
                                    html.Div(  # Crash cause container
                                        [
                                            html.Div(
                                                [
                                                    html.H6(
                                                        children="Top Crash Causes"
                                                    ),
                                                    html.A(
                                                        'Download Data',
                                                        id='cause_download',
                                                        download='crash_cause.csv',
                                                        href="",
                                                        target="_blank",
                                                        style={"float": "right",}
                                                        )
                                                    ]
                                                ),
                                            dcc.Graph(id="CrashCauseGraph"),
                                        ],
                                        className="bare_container seven columns",
                                        title="Top Crash Causes",
                                    ),
                                    html.Div(
                                        dash_table.DataTable(
                                            columns=[
                                                {
                                                    "name": "Geography",
                                                    "id": "Geography",
                                                },
                                                {"name": "Year", "id": "Year"},
                                                {
                                                    "name": "Crash Type",
                                                    "id": "Selected Crash Type",
                                                },
                                            ],
                                            id="CauseTable_1",
                                        ),
                                        className="hidden",
                                    ),
                                    html.Div(
                                        dash_table.DataTable(
                                            columns=[
                                                {
                                                    "name": "Top Crash Causes",
                                                    "id": "Crash Cause",
                                                },
                                                {
                                                    "name": "Number of Crashes",
                                                    "id": "Cause Number of Crashes",
                                                },
                                            ],
                                            id="CauseTable_2",
                                        ),
                                        className="hidden",
                                    ),
                                    html.Div(  # Daily distribution cntainer
                                        [
                                            html.Div(
                                                [
                                                    html.H6(
                                                        children="Daily Distribution"
                                                    ),
                                                    html.A(
                                                        'Download Data',
                                                        id='daily_download',
                                                        download='daily.csv',
                                                        href="",
                                                        target="_blank",
                                                        style={"float": "right",}
                                                        )
                                                    ]
                                                ),
                                            dcc.Graph(id="DailyGraph"),
                                        ],
                                        className="bare_container five columns",
                                        title="Daily Distribution",
                                    ),
                                    html.Div(
                                        dash_table.DataTable(
                                            columns=[
                                                {
                                                    "name": "Geography",
                                                    "id": "Geography",
                                                },
                                                {"name": "Year", "id": "Year"},
                                                {
                                                    "name": "Crash Type",
                                                    "id": "Selected Crash Type",
                                                },
                                            ],
                                            id="DailyTable_1",
                                        ),
                                        className="hidden",
                                    ),
                                    html.Div(
                                        dash_table.DataTable(
                                            columns=[
                                                {"name": "Hour", "id": "Hour"},
                                                {
                                                    "name": "Number of Crashes",
                                                    "id": "Daily Number of Crashes",
                                                },
                                            ],
                                            id="DailyTable_2",
                                        ),
                                        className="hidden",
                                    ),
                                ],
                                className="row",
                            ),
                            html.Div(  # Pie charts container
                                [
                                    html.Div(  # Weather condition container
                                        [
                                            html.Div(
                                                [
                                                    html.H6(
                                                        children="Weather Condition"
                                                    ),
                                                    html.A(
                                                        'Download Data',
                                                        id='weather_download',
                                                        download='weather.csv',
                                                        href="",
                                                        target="_blank",
                                                        style={"float": "right",}
                                                        )
                                                    ]
                                                ),
                                            dcc.Graph(id="WeatherGraph"),
                                        ],
                                        className="bare_container threehalf columns",
                                        title="Weather Condition",
                                    ),
                                    html.Div(
                                        dash_table.DataTable(
                                            columns=[
                                                {
                                                    "name": "Geography",
                                                    "id": "Geography",
                                                },
                                                {"name": "Year", "id": "Year"},
                                                {
                                                    "name": "Crash Type",
                                                    "id": "Selected Crash Type",
                                                },
                                            ],
                                            id="WeatherTable_1",
                                        ),
                                        className="hidden",
                                    ),
                                    html.Div(
                                        dash_table.DataTable(
                                            columns=[
                                                {
                                                    "name": "Weather Condition",
                                                    "id": "Weather Condition",
                                                },
                                                {
                                                    "name": "Share of Crashes",
                                                    "id": "Weather Share of Crashes",
                                                },
                                            ],
                                            id="WeatherTable_2",
                                        ),
                                        className="hidden",
                                    ),
                                    html.Div(  # Light condition container
                                        [
                                            html.Div(
                                                [
                                                    html.H6(
                                                        children="Light Condition"
                                                    ),
                                                    html.A(
                                                        'Download Data',
                                                        id='light_download',
                                                        download='light.csv',
                                                        href="",
                                                        target="_blank",
                                                        style={"float": "right",}
                                                        )
                                                    ]
                                                ),
                                            dcc.Graph(id="LightingGraph"),
                                        ],
                                        className="bare_container threehalf columns",
                                        title="Light Condition",
                                    ),
                                    html.Div(
                                        dash_table.DataTable(
                                            columns=[
                                                {
                                                    "name": "Geography",
                                                    "id": "Geography",
                                                },
                                                {"name": "Year", "id": "Year"},
                                                {
                                                    "name": "Crash Type",
                                                    "id": "Selected Crash Type",
                                                },
                                            ],
                                            id="LightTable_1",
                                        ),
                                        className="hidden",
                                    ),
                                    html.Div(
                                        dash_table.DataTable(
                                            columns=[
                                                {
                                                    "name": "Light Condition",
                                                    "id": "Light Condition",
                                                },
                                                {
                                                    "name": "Share of Crashes",
                                                    "id": "Light Share of Crashes",
                                                },
                                            ],
                                            id="LightTable_2",
                                        ),
                                        className="hidden",
                                    ),
                                    html.Div(  # Road surface container
                                        [
                                            html.Div(
                                                [
                                                    html.H6(
                                                        children="Road Surface Condition"
                                                    ),
                                                    html.A(
                                                        'Download Data',
                                                        id='surface_download',
                                                        download='surface.csv',
                                                        href="",
                                                        target="_blank",
                                                        style={"float": "right",}
                                                        )
                                                    ]
                                                ),
                                            dcc.Graph(id="RoadSurfaceGraph"),
                                        ],
                                        className="bare_container threehalf columns",
                                        title="Road Surface Condition",
                                    ),
                                    html.Div(
                                        dash_table.DataTable(
                                            columns=[
                                                {
                                                    "name": "Geography",
                                                    "id": "Geography",
                                                },
                                                {"name": "Year", "id": "Year"},
                                                {
                                                    "name": "Crash Type",
                                                    "id": "Selected Crash Type",
                                                },
                                            ],
                                            id="SurfaceTable_1",
                                        ),
                                        className="hidden",
                                    ),
                                    html.Div(
                                        dash_table.DataTable(
                                            columns=[
                                                {
                                                    "name": "Road Surface Condition",
                                                    "id": "Surface Condition",
                                                },
                                                {
                                                    "name": "Share of Crashes",
                                                    "id": "Surface Share of Crashes",
                                                },
                                            ],
                                            id="SurfaceTable_2",
                                        ),
                                        className="hidden",
                                    ),
                                ],
                                className="row",
                            ),
                        ],
                        className="eight columns",
                    ),
                    html.Div(  # Maps container
                        [
                            html.Div(  # Crash Map container
                                [
                                    html.Div(
                                        [
                                            html.H6(children="Crash Map"),
                                            dcc.Graph(
                                                id="map-graph", animate=True
                                            ),
                                        ],
                                        className="bare_container twelve columns",
                                    )
                                ],
                                className="row",
                                title="Crash Location Map",
                            ),
                            html.Div(  # Crash Risk Map container
                                [
                                    html.Div(
                                        [
                                            html.H6(children="Crash Risk Map"),
                                            html.A(
                                                'Learn More',
                                                href='https://gitlab.com/ccrpc/cu-safety-forecasting-tool',
                                                target='_blank',
                                                style={"float": "right",}
                                            ),
                                            dcc.Graph(
                                                id="risk-map-graph-2",
                                                figure=gen_prediction_point_map(df_prediction_dropna),
                                                animate=True,
                                            ),
                                        ],
                                        className="bare_container twelve columns",
                                        style={"margin-top": 5},
                                    )
                                ],
                                className="row",
                                title="Crash Risk Map",
                            ),
                        ],
                        className="four columns",
                    ),
                ],
                className="row",
            ),
        ]
    )
)

# callback funtion to update total crashes
@app.callback(
    Output("TotalCrashText", "children"),
    [
        Input("Geography", "value"),
        Input("Year", "value"),
        Input('session-id', 'children')
        ]
    )

def get_total_crashes(selected_Geography, selected_Year, session_id):
    """Update the total crashes number based on Geography and Year filter values

    Args:
        selected_Geography (str): Chamapign County, University District, municipalities, Urban (Within MPA boundary), Rural (Outside of MPA boundary)
        selected_Year (int): start_yr - end_yr
        session_id: user session

    Returns:
        int: total number of crashes for selected geography and year

    """
    dff = get_dataframe(selected_Geography, selected_Year, session_id)
    return dff.shape[0]

# callback funtion to update total crash datatable
@app.callback(
    Output("TotalCrashTable", "data"),
    [
        Input("Geography", "value"),
        Input("Year", "value"),
        Input("TotalCrashText", "children"),
    ]
)
def udpate_total_crash_table(
    selected_Geography, selected_Year, TotalCrashText
):
    """Update the total crashes number table based on Geography and Year filter values

    Args:
        selected_Geography (str): Chamapign County, University District, municipalities, Urban (Within MPA boundary), Rural (Outside of MPA boundary)
        selected_Year (int): start_yr - end_yr
        TotalCrashText: displayed total crashes numebr

    Returns:
        obj: total number of crashes for selected geography and year to be used in hidden datable

    """
    data = [
        {
            "Geography": selected_Geography,
            "Year": selected_Year,
            "Total number of crashes": TotalCrashText,
        }
    ]
    return data

# callback funtion to update fatalities
@app.callback(
    Output("FatalitiesText", "children"),
    [
        Input("Geography", "value"),
        Input("Year", "value"),
        Input('session-id', 'children')
        ]
    )
def get_fatalities(selected_Geography, selected_Year,session_id):
    """Update the total fatalities number based on Geography and Year filter values

    Args:
        selected_Geography (str): Chamapign County, University District, municipalities, Urban (Within MPA boundary), Rural (Outside of MPA boundary)
        selected_Year (int): start_yr - end_yr
        session_id: user session

    Returns:
        int: total number of fatalities for selected geography and year

    """
    dff = get_dataframe(selected_Geography, selected_Year, session_id)
    return dff['TotalFatals'].sum()

# callback funtion to update total fatalities datatable
@app.callback(
    Output("FatalitiesTable", "data"),
    [
        Input("Geography", "value"),
        Input("Year", "value"),
        Input("FatalitiesText", "children"),
    ],
)
def udpate_total_fatalities_table(
    selected_Geography, selected_Year, FatalitiesText
):
    """Update the total fatalities number table based on Geography and Year filter values

    Args:
        selected_Geography (str): Chamapign County, University District, municipalities, Urban (Within MPA boundary), Rural (Outside of MPA boundary)
        selected_Year (int): start_yr - end_yr
        FatalitiesText: displayed total fatalities number

    Returns:
        obj: total number of fatalities for selected geography and year to be used in hidden datable

    """
    data = [
        {
            "Geography": selected_Geography,
            "Year": selected_Year,
            "Total number of fatalities": FatalitiesText,
        }
    ]
    return data

# callback funtion to update A injuries
@app.callback(
    Output("SevereInjuriesText", "children"),
    [
        Input("Geography", "value"),
        Input("Year", "value"),
        Input('session-id', 'children')
        ]
       )
def get_a_injuries(selected_Geography, selected_Year,session_id):
    """Update the total severe injuries number based on Geography and Year filter values

    Args:
        selected_Geography (str): Chamapign County, University District, municipalities, Urban (Within MPA boundary), Rural (Outside of MPA boundary)
        selected_Year (int): start_yr - end_yr
        session_id: user session

    Returns:
        int: total number of severe injuries for selected geography and year

    """
    dff = get_dataframe(selected_Geography, selected_Year, session_id)
    return dff['Ainjuries'].sum()

# callback funtion to update total severe injuries datatable
@app.callback(
    Output("SevereInjuriesTable", "data"),
    [
        Input("Geography", "value"),
        Input("Year", "value"),
        Input("SevereInjuriesText", "children"),
    ],
)
def udpate_a_injuries_crash_table(
    selected_Geography, selected_Year, SevereInjuriesText
):
    """Update the total severe injuries number table based on Geography and Year filter values

    Args:
        selected_Geography (str): Chamapign County, University District, municipalities, Urban (Within MPA boundary), Rural (Outside of MPA boundary)
        selected_Year (int): start_yr - end_yr
        SevereInjuriesText: displayed total severe injuries number

    Returns:
        obj: total number of severe injuries for selected geography and year to be used in hidden datable

    """
    data = [
        {
            "Geography": selected_Geography,
            "Year": selected_Year,
            "Total number of severe injuries": SevereInjuriesText,
        }
    ]
    return data

# callback funtion to update Bike Crashes
@app.callback(
    Output("BikeCrashesText", "children"),
    [
        Input("Geography", "value"),
        Input("Year", "value"),
        Input('session-id', 'children')
        ]
)
def get_bike_crashes(selected_Geography, selected_Year,session_id):
    """Update the total bike crashes number based on Geography and Year filter values

    Args:
        selected_Geography (str): Chamapign County, University District, municipalities, Urban (Within MPA boundary), Rural (Outside of MPA boundary)
        selected_Year (int): start_yr - end_yr
        session_id: user session

    Returns:
        int: total number of bike crashes for selected geography and year

    """
    dff = get_dataframe(selected_Geography, selected_Year, session_id)
    return dff[(dff.TypeofFirstCrash == 'Pedalcyclist')].shape[0]

# callback funtion to update total bike crashes datatable
@app.callback(
    Output("BikeCrashesTable", "data"),
    [
        Input("Geography", "value"),
        Input("Year", "value"),
        Input("BikeCrashesText", "children"),
    ],
)
def udpate_bike_crash_table(
    selected_Geography, selected_Year, BikeCrashesText
):
    """Update the total bike crashes number table based on Geography and Year filter values

    Args:
        selected_Geography (str): Chamapign County, University District, municipalities, Urban (Within MPA boundary), Rural (Outside of MPA boundary)
        selected_Year (int): start_yr - end_yr
        BikeCrashesText: displayed total bike/ped crashes number

    Returns:
        obj: total number of bike crashes for selected geography and year to be used in hidden datable

    """
    data = [
        {
            "Geography": selected_Geography,
            "Year": selected_Year,
            "Total number of bicylle crashes": BikeCrashesText,
        }
    ]
    return data

# callback funtion to update Ped Crashes
@app.callback(
    Output('PedCrashesText', 'children'),
    [
     Input('Geography', 'value'),
     Input('Year', 'value'),
     Input('session-id', 'children')
     ]
    )
def get_ped_crashes(selected_Geography,selected_Year,session_id):
    """Update the total ped crashes number based on Geography and Year filter values

    Args:
        selected_Geography (str): Chamapign County, University District, municipalities, Urban (Within MPA boundary), Rural (Outside of MPA boundary)
        selected_Year (int): start_yr - end_yr
        session_id: user session

    Returns:
        int: total number of ped crashes for selected geography and year

    """
    dff = get_dataframe(selected_Geography, selected_Year, session_id)
    return dff[(dff.TypeofFirstCrash == 'Pedestrian')].shape[0]

# callback funtion to update total ped crashes datatable
@app.callback(
    Output('PedCrashesTable', 'data'),
    [
     Input('Geography', 'value'),
     Input('Year', 'value'),
     Input ('PedCrashesText', 'children')
     ]
    )
def udpate_ped_crash_table (
                            selected_Geography,selected_Year,PedCrashesText
                            ):
    """Update the total ped crashes number table based on Geography and Year filter values

    Args:
        selected_Geography (str): Chamapign County, University District, municipalities, Urban (Within MPA boundary), Rural (Outside of MPA boundary)
        selected_Year (int): start_yr - end_yr
        PedCrashesText: displayed total ped crashes number

    Returns:
        obj: total number of ped crashes for selected geography and year to be used in hidden datable

    """
    data=[{
            'Geography':selected_Geography,
            'Year':selected_Year,
            'Total number of pedestrian crashes':PedCrashesText}]
    return data

# callback funtion to update Heavy vehicle Crashes
@app.callback(
    Output('HeavyCrashesText', 'children'),
    [
     Input('Geography', 'value'),
     Input('Year', 'value'),
     Input('session-id', 'children')
     ]
    )
def get_heavy_crashes(selected_Geography,selected_Year,session_id):
    """Update the total Heavy vehicle crashes number based on Geography and Year filter values

    Args:
        selected_Geography (str): Chamapign County, University District, municipalities, Urban (Within MPA boundary), Rural (Outside of MPA boundary)
        selected_Year (int): start_yr - end_yr
        session_id: user session

    Returns:
        int: total number of heavy vehicle crashes for selected geography and year

    """
    dff = get_dataframe(selected_Geography, selected_Year, session_id)
    return sum(dff['heavy'])

# callback funtion to update total Heavy vehicle crashes datatable
@app.callback(
    Output('HeavyCrashesTable', 'data'),
    [
     Input('Geography', 'value'),
     Input('Year', 'value'),
     Input ('HeavyCrashesText', 'children')
     ]
    )
def udpate_heavy_crash_table (
                              selected_Geography,selected_Year,HeavyCrashesText
                              ):
    """Update the total heavy vehicle crashes number table based on Geography and Year filter values

    Args:
        selected_Geography (str): Chamapign County, University District, municipalities, Urban (Within MPA boundary), Rural (Outside of MPA boundary)
        selected_Year (int): start_yr - end_yr
        HeavyCrashesText: displayed total heavy vehicle crashes number

    Returns:
        obj: total number of heavy vehicle crashes for selected geography and year to be used in hidden datable

    """
    data=[
          {
            'Geography':selected_Geography,
            'Year':selected_Year,
            'Total number of heavy vehicle crashes':HeavyCrashesText
            }
          ]
    return data

def stat_highlight():
    """Define html component className for selected crash statistics

    Args:

    Returns:
        tuple: six crash statistics box className

    """
    stat_total_highlight=(
        "bare_container_active one columns",
        "bare_container one columns",
        "bare_container one columns",
        "bare_container one columns",
        "bare_container one columns",
        "bare_container one columns"
        )
    stat_fatal_highlight=(
        "bare_container one columns" ,
        "bare_container_active one columns",
        "bare_container one columns" ,
        "bare_container one columns" ,
        "bare_container one columns" ,
        "bare_container one columns"
        )
    stat_injury_highlight=(
        "bare_container one columns" ,
        "bare_container one columns",
        "bare_container_active one columns" ,
        "bare_container one columns" ,
        "bare_container one columns" ,
        "bare_container one columns"
        )
    stat_bike_highlight=(
        "bare_container one columns" ,
        "bare_container one columns",
        "bare_container one columns" ,
        "bare_container_active one columns" ,
        "bare_container one columns",
        "bare_container one columns"
        )
    stat_ped_highlight=(
        "bare_container one columns" ,
        "bare_container one columns",
        "bare_container one columns" ,
        "bare_container one columns" ,
        "bare_container_active one columns" ,
        "bare_container one columns"
        )
    stat_heavy_highlight=(
        "bare_container one columns" ,
        "bare_container one columns",
        "bare_container one columns" ,
        "bare_container one columns" ,
        "bare_container one columns" ,
        "bare_container_active one columns"
        )
    stat_no_highlight= (
        "bare_container one columns" ,
        "bare_container one columns",
        "bare_container one columns" ,
        "bare_container one columns" ,
        "bare_container one columns" ,
        "bare_container one columns"
        )
    return (
        stat_total_highlight,
        stat_fatal_highlight,
        stat_injury_highlight,
        stat_bike_highlight,
        stat_ped_highlight,
        stat_heavy_highlight,
        stat_no_highlight
        )

# Highlight button when clicked
@app.callback(
    [
     Output('TotalCrash', 'className'),
     Output('TotalFatalities', 'className'),
     Output('SevereInjuries', 'className'),
     Output('BikeCrashes', 'className'),
     Output('PedCrashes', 'className'),
     Output('HeavyCrashes', 'className'),
    ],
    [
     Input('total_crash_click','n_clicks'),
     Input('fatality_click','n_clicks'),
     Input('injury_click','n_clicks'),
     Input('bike_click','n_clicks'),
     Input('ped_click','n_clicks'),
     Input('heavy_click','n_clicks')
     ]
)
def set_active(
    total_crash_click,
    fatality_click,
    injury_click,
    bike_click,
    ped_click,
    heavy_click
    ):
    """Highlight crash statistics box based on most recent click

    Args:
        number of clicks on the crash statistics boxes

    Returns:
        tuple: ClassName combination for the statistics boxes

    """
    changed_id='total_crash_click'
    changed_id = [p['prop_id'] for p in dash.callback_context.triggered][0]
    value=[p['value'] for p in dash.callback_context.triggered][0]
    if 'heavy_click' in changed_id and value==0:
        return stat_highlight()[0]
    elif 'total_crash_click' in changed_id:
        return stat_highlight()[0]
    elif 'fatality_click' in changed_id:
        return stat_highlight()[1]
    elif 'injury_click' in changed_id:
        return stat_highlight()[2]
    elif 'bike_click' in changed_id:
        return stat_highlight()[3]
    elif 'ped_click' in changed_id:
        return stat_highlight()[4]
    elif 'heavy_click' in changed_id:
        return stat_highlight()[5]
    else:
        return stat_highlight()[6]

# Function to filter data based on statistics box selected
def filter_stat(df):
    """Fiter dataframe based on statistics boxes clicked

    Args:
        df: crash dataframe, filtered by geogrphy and year
        changed_id: statistics box clicked

    Returns:
        pandas dataframe: filtered dataframe

    """
    df_stat=df
    changed_id = [p['prop_id'] for p in dash.callback_context.triggered][0]
    if 'total_crash_click' in changed_id:
        df_stat=df
    if 'fatality_click' in changed_id:
        df_stat=df[df.TotalFatals>0]
    if 'injury_click' in changed_id:
        df_stat=df[df.Ainjuries>0]
    if 'bike_click' in changed_id:
        df_stat=df[(df.TypeofFirstCrash == 'Pedalcyclist')]
    if 'ped_click' in changed_id:
        df_stat=df[(df.TypeofFirstCrash == 'Pedestrian')]
    if 'heavy_click' in changed_id:
        df_stat=df[df.heavy>0]
    return df_stat

# callback funtion to update crash types graph
@app.callback(
    Output("CrashTypeGraph", "figure"),
    [
        Input("Geography", "value"),
        Input("CrashTypeGraph", "clickData"),
        Input("Year", "value"),
        Input('total_crash_click','n_clicks'),
        Input('fatality_click','n_clicks'),
        Input('injury_click','n_clicks'),
        Input('bike_click','n_clicks'),
        Input('ped_click','n_clicks'),
        Input('heavy_click','n_clicks'),
        Input('session-id', 'children')
        ]
    )
def update_crash_type_graph(
    selected_Geography,
    clickData,
    selected_Year,
    total_crash_click,
    fatality_click,
    injury_click,
    bike_click,
    ped_click,
    heavy_click,
    session_id
    ):
    """Update the top 8 crash types based on Geography and Year filter values. Bar color changes upon selected individual crash type

    Args:
        selected_Geography (str): Chamapign County, University District, municipalities, Urban (Within MPA boundary), Rural (Outside of MPA boundary)
        clickData: user selected individual crash type
        selected_Year (int): start_yr - end_yr
        session_id: user session
        fatality_click & stat_total_highlight, etc.: wheter the fatality or other statistics is selected

    Returns:
        figure: horizontal bar chart of top 8 crash types

    """
    data = []
    color = ["#1F77B4"] * 8
    df_filter = get_dataframe(
        selected_Geography,
        selected_Year,
        session_id
        )
    try:
        df_stat=filter_stat(df_filter)
        CrashTypes=(
            df_stat['TypeofFirstCrash']
            .value_counts(sort=True, ascending=True)
            .to_frame()
            )
        if CrashTypes.shape[0]>=8:
            TopCrashTypes=(
                df_stat['TypeofFirstCrash']
                .value_counts(sort=True, ascending=True)
                .to_frame()
                .tail(8)
                )
        if  CrashTypes.shape[0]<8:
            TopCrashTypes=CrashTypes
        data.append(
            {
                "x": TopCrashTypes["TypeofFirstCrash"].to_list(),
                "y": TopCrashTypes.index.to_list(),
                "type": "bar",
                "marker": {"color": color},
                "orientation": "h",
            }
        )
        figure = {
            "data": data,
            "layout": {
                "margin": {"l": 180, "t": 5, "r": 0, "b": 60},
                "height": 240,
                "xaxis": dict(
                    title="Number of crashes",
                    titlefont=dict(size=12, color="#7f7f7f"),
                ),
                "yaxis": dict(
                    titlefont=dict(size=12, color="#7f7f7f"),
                    tickfont={"size": "11"},
                ),
            },
        }
        if clickData is not None:
            color_index=(
                         figure['data'][0]['y']
                         .index(clickData['points'][0]['y'])
                         )
            figure['data'][0]['marker']['color'][color_index] = '#144c73'
        return figure
    except ValueError:
        return {}

# callback funtion to update top crash types table_1 (municipality & year)
@app.callback(
    Output("CrashTypeTable_1", "data"),
    [Input("Geography", "value"), Input("Year", "value")],
)
def udpate_crash_type_table_1(selected_Geography, selected_Year):
    """Update the top crash types table_1 municipality & year info

    Args:
        selected_Geography (str): Chamapign County, University District, municipalities, Urban (Within MPA boundary), Rural (Outside of MPA boundary)
        selected_Year (int): start_yr - end_yr

    Returns:
        obj: selected geography and year to be used in hidden datable

    """
    data = [{"Geography": selected_Geography, "Year": selected_Year}]
    return data

# callback funtion to update top crash types table_2 (crash type and number of crashes)
@app.callback(
    Output("CrashTypeTable_2", "data"),
    [Input("CrashTypeGraph", "figure")]
)
def udpate_crash_type_table_2(crash_types):
    """Update the top crash types table_2 top crash types number of crashes

    Args:
        crash_types (obj): filtered top crash types

    Returns:
        obj: filtered top crash types and number of crashes rows

    """
    data = []
    try :
        for i in range (0,len(crash_types['data'][0]['x'])):
            data.append(
                {
                    'Crash Type':crash_types['data'][0]['y'][i],
                    'Type Number of Crashes': crash_types['data'][0]['x'][i]
                    }
                )
        return data
    except:
        return data

@app.callback(
    Output('type_download', 'href'),
    [Input("CrashTypeTable_2", "data")])
def update_download_link(data):
    download_data=pd.DataFrame(data)
    csv_string = download_data.to_csv(index=False, encoding='utf-8')
    csv_string = "data:text/csv;charset=utf-8," + urllib.parse.quote(csv_string)
    return csv_string

# function to filter data
def filter_dataframe(df, CrashTypeSelected):
    """Filter crash data table based on crash type the user selected on the Top Crash Types bar chart

    Args:
        df (pands DataFrame): crashes_2017_2021.csv
        CrashTypeSelected (str): user selected individual crash type

    Returns:
        dff (pands DataFrame): filtered crash datatable

    """
    if CrashTypeSelected == "null":
        dff = df
    else:
        dff = df[(df.TypeofFirstCrash == CrashTypeSelected)]
    return dff

# callback funtion to update crash causes graph
@app.callback(
    Output("CrashCauseGraph", "figure"),
    [
        Input("Geography", "value"),
        Input("CrashTypeGraph", "clickData"),
        Input("Year", "value"),
        Input('total_crash_click','n_clicks'),
        Input('fatality_click','n_clicks'),
        Input('injury_click','n_clicks'),
        Input('bike_click','n_clicks'),
        Input('ped_click','n_clicks'),
        Input('heavy_click','n_clicks'),
        Input('session-id', 'children')
        ]
    )
def update_crash_cause_graph(
    selected_Geography,
    clickData,
    selected_Year,
    total_crash_click,
    fatality_click,
    injury_click,
    bike_click,
    ped_click,
    heavy_click,
    session_id
):
    """Update the top 8 main crash causes based on Geography and Year filter values and user selected individual main crash type

    Args:
        selected_Geography (str): Chamapign County, University District, municipalities, Urban (Within MPA boundary), Rural (Outside of MPA boundary)
        clickData: user selected individual crash type
        selected_Year (int): start_yr - end_yr
        session_id: user session
        fatality_click & stat_total_highlight, etc.: wheter the fatality or other statistics is selected

    Returns:
        figure: horizontal bar chart of top 8 main crash causes

    """
    data = []
    color = "#b296c7"
    df_filter = get_dataframe(selected_Geography, selected_Year, session_id)
    df_stat=filter_stat(df_filter)
    if not clickData:
        dfff=df_stat
    else:
        CrashTypeSelected=(
            ast.literal_eval(json.dumps(clickData, indent=2))
           .get("points")[0]
           .get('y')
           )
        dfff = filter_dataframe(df_stat, CrashTypeSelected)
    CrashCause=(
        dfff['Cause1']
        .value_counts(sort=True, ascending=True)
        .to_frame()
        )
    if CrashCause.shape[0]>=8:
        TopCrashCause=CrashCause.tail(8)
    else:
        TopCrashCause=CrashCause
    data.append(
        {
            "x": TopCrashCause["Cause1"].to_list(),
            "y": TopCrashCause.index.to_list(),
            "type": "bar",
            "marker": {"color": color},
            "orientation": "h",
        }
    )
    figure = {
        "data": data,
        "layout": {
            "margin": {"l": 200, "t": 5, "r": 0, "b": 60},
            "height": 240,
            "xaxis": dict(
                title="Number of crashes",
                titlefont=dict(size=12, color="#7f7f7f"),
            ),
            "yaxis": dict(
                titlefont=dict(size=12, color="#7f7f7f"),
                tickfont={"size": "11"},
            ),
        },
    }
    return figure

# callback funtion to update cause table_1 (municipality, year, selected crash type)
@app.callback(
    Output("CauseTable_1", "data"),
    [
        Input("Geography", "value"),
        Input("Year", "value"),
        Input("CrashTypeGraph", "clickData"),
    ],
    )
def udpate_cause_table_1(
    selected_Geography, selected_Year, selected_crash_type
    ):
    """Update the cause table_1 municipality, year, selected crash type info

    Args:
        selected_Geography (str): Chamapign County, University District, municipalities, Urban (Within MPA boundary), Rural (Outside of MPA boundary)
        selected_Year (int): start_yr - end_yr
        selected_crash_type (obj): selected crash type by user

    Returns:
        obj: selected geography, year and crash type info to be used in hidden datable

    """
    if not selected_crash_type:
        data = [
            {
                "Geography": selected_Geography,
                "Year": selected_Year,
                "Selected Crash Type": "All",
            }
        ]
    else:
        data = [
            {
                "Geography": selected_Geography,
                "Year": selected_Year,
                "Selected Crash Type": ast.literal_eval(
                    json.dumps(selected_crash_type, indent=2)
                )
                .get("points")[0]
                .get("y"),
            }
        ]
    return data

# callback funtion to update cause table_2 (crash cause, number of crashes)
@app.callback(
    Output("CauseTable_2", "data"), [Input("CrashCauseGraph", "figure")]
)
def udpate_cause_table_2(crash_cause):
    """Update the crash cause table_2 info

    Args:
        crash_cause (obj): crash cause graph

    Returns:
        obj: cause and number of crashes

    """
    data = []
    try:
        for i in range(0,len(crash_cause['data'][0]['x'])):
            data.append(
                {
                    "Crash Cause": crash_cause["data"][0]["y"][i],
                    "Cause Number of Crashes": crash_cause["data"][0]["x"][i],
                }
            )
    except IndexError:
        pass
    return data

@app.callback(
    Output('cause_download', 'href'),
    [Input("CauseTable_2", "data")])
def update_download_link(data):
    download_data=pd.DataFrame(data)
    csv_string = download_data.to_csv(index=False, encoding='utf-8')
    csv_string = "data:text/csv;charset=utf-8," + urllib.parse.quote(csv_string)
    return csv_string

# callback funtion to update monthly distribution graph
@app.callback(
    Output("MonthlyGraph", "figure"),
    [
        Input("Geography", "value"),
        Input("CrashTypeGraph", "clickData"),
        Input("Year", "value"),
        Input('total_crash_click','n_clicks'),
        Input('fatality_click','n_clicks'),
        Input('injury_click','n_clicks'),
        Input('bike_click','n_clicks'),
        Input('ped_click','n_clicks'),
        Input('heavy_click','n_clicks'),
        Input('session-id', 'children')
    ],
    )
def update_monthly_graph(
    selected_Geography,
    clickData,
    selected_Year,
    total_crash_click,
    fatality_click,
    injury_click,
    bike_click,
    ped_click,
    heavy_click,
    session_id
    ):
    """Update monthly distribution chart based on Geography and Year filter values and user selected individual main crash type

    Args:
        selected_Geography (str): Chamapign County, University District, municipalities, Urban (Within MPA boundary), Rural (Outside of MPA boundary)
        clickData: user selected individual crash type
        selected_Year (int): start_yr - end_yr
        session_id: user session
        fatality_click & stat_total_highlight, etc.: wheter the fatality or other statistics is selected

    Returns:
        figure: monthly distribution line chart

    """
    data = []
    df_filter = get_dataframe(selected_Geography, selected_Year, session_id)
    df_stat=filter_stat(df_filter)
    if not clickData:
        dfff=df_stat
    else:
        CrashTypeSelected=(
            ast.literal_eval(json.dumps(clickData, indent=2))
           .get("points")[0]
           .get('y')
           )
        dfff = filter_dataframe(df_stat, CrashTypeSelected)
    Monthly=dfff['CrashMonth'].value_counts().sort_index().to_frame()
    data.append(
        {
            "y": Monthly["CrashMonth"].to_list(),
            "x": Monthly.index.to_list(),
            "type": "line",
            "mode": "lines+markers",
        }
    )
    figure = {
        "data": data,
        "layout": {
            "margin": {"t": 5, "l": 50, "r": 10, "b": 60},
            "height": 240,
            "xaxis": dict(
                title="Month", titlefont=dict(size=12, color="#7f7f7f")
            ),
            "yaxis": dict(
                title="Number of crashes",
                titlefont=dict(size=12, color="#7f7f7f"),
                tickfont={"size": "11"},
                showline=True,
            ),
        },
    }
    return figure

# callback funtion to update monthly table_1 (municipality, year, selected crash type)
@app.callback(
    Output("MonthlyTable_1", "data"),
    [
        Input("Geography", "value"),
        Input("Year", "value"),
        Input("CrashTypeGraph", "clickData"),
    ],
    )
def udpate_monthly_table_1(
    selected_Geography, selected_Year, selected_crash_type
    ):
    """Update the monthly table_1 municipality, year, selected crash type info

    Args:
        selected_Geography (str): Chamapign County, University District, municipalities, Urban (Within MPA boundary), Rural (Outside of MPA boundary)
        selected_Year (int): start_yr - end_yr
        selected_crash_type (obj): selected crash type by user

    Returns:
        obj: selected geography, year and crash type info to be used in hidden datable

    """
    if not selected_crash_type:
        data = [
            {
                "Geography": selected_Geography,
                "Year": selected_Year,
                "Selected Crash Type": "All",
            }
        ]
    else:
        data = [
            {
                "Geography": selected_Geography,
                "Year": selected_Year,
                "Selected Crash Type": ast.literal_eval(
                    json.dumps(selected_crash_type, indent=2)
                    )
                .get("points")[0]
                .get("y"),
            }
        ]
    return data

# callback funtion to update monthly table_2 (month, number of crashes)
@app.callback(
    Output("MonthlyTable_2", "data"), [Input("MonthlyGraph", "figure")]
)
def udpate_monthly_table_2(monthly):
    """Update the monthly table_2 month and number of crashes info

    Args:
        monthly (obj): monthly crash graph

    Returns:
        obj: month and number of crashes

    """
    data = []
    try:
        for i in range(12):
            data.append(
                {
                    "Month": monthly["data"][0]["x"][i],
                    "Monthly Number of Crashes": monthly["data"][0]["y"][i],
                }
            )
    except IndexError:
        pass
    return data

@app.callback(
    Output('monthly_download', 'href'),
    [Input("MonthlyTable_2", "data")])
def update_download_link(data):
    download_data=pd.DataFrame(data)
    csv_string = download_data.to_csv(index=False, encoding='utf-8')
    csv_string = "data:text/csv;charset=utf-8," + urllib.parse.quote(csv_string)
    return csv_string

# callback funtion to update daily distribution graph
@app.callback(
    Output("DailyGraph", "figure"),
    [
        Input("Geography", "value"),
        Input("CrashTypeGraph", "clickData"),
        Input('Year', 'value'),
        Input('total_crash_click','n_clicks'),
        Input('fatality_click','n_clicks'),
        Input('injury_click','n_clicks'),
        Input('bike_click','n_clicks'),
        Input('ped_click','n_clicks'),
        Input('heavy_click','n_clicks'),
        Input('session-id', 'children')
    ],
)
def update_daily_graph(
    selected_Geography,
    clickData,
    selected_Year,
    total_crash_click,
    fatality_click,
    injury_click,
    bike_click,
    ped_click,
    heavy_click,
    session_id
    ):
    """Update daily distribution chart based on Geography and Year filter values and user selected individual main crash type

    Args:
        selected_Geography (str): Chamapign County, University District, municipalities, Urban (Within MPA boundary), Rural (Outside of MPA boundary)
        clickData: user selected individual crash type
        selected_Year (int): start_yr - end_yr
        session_id: user session
        fatality_click & stat_total_highlight, etc.: wheter the fatality or other statistics is selected

    Returns:
        figure: daily distribution line chart

    """
    data = []
    color = "#b296c7"
    df_filter = get_dataframe(selected_Geography, selected_Year, session_id)
    df_stat=filter_stat(df_filter)
    if not clickData:
        dfff=df_stat
    else:
        CrashTypeSelected=(
            ast.literal_eval(
                json.dumps(clickData, indent=2)
                )
            .get("points")[0]
            .get('y')
            )
        dfff = filter_dataframe(df_stat, CrashTypeSelected)
    Daily=dfff['CrashHour'].value_counts().sort_index().to_frame()
    data.append(
        {
            "y": Daily["CrashHour"].to_list(),
            "x": Daily.index.to_list(),
            "type": "line",
            "marker": {"color": color},
            "mode": "lines+markers",
        }
    )
    figure = {
        "data": data,
        "layout": {
            "margin": {"t": 5, "l": 50, "r": 10, "b": 60},
            "height": 240,
            "xaxis": dict(
                range=[0, 24],
                title="Hour",
                titlefont=dict(size=12, color="#7f7f7f"),
            ),
            "yaxis": dict(
                title="Number of crashes",
                titlefont=dict(size=12, color="#7f7f7f"),
                tickfont={"size": "11"},
                showline=True,
            ),
        },
    }
    return figure

# callback funtion to update daily table_1 (municipality, year, selected crash type)
@app.callback(
    Output("DailyTable_1", "data"),
    [
        Input("Geography", "value"),
        Input("Year", "value"),
        Input("CrashTypeGraph", "clickData"),
    ],
)
def udpate_daily_table_1(
    selected_Geography, selected_Year, selected_crash_type
):
    """Update the daily table_1 municipality, year, selected crash type info

    Args:
        selected_Geography (str): Chamapign County, University District, municipalities, Urban (Within MPA boundary), Rural (Outside of MPA boundary)
        selected_Year (int): start_yr - end_yr
        selected_crash_type (obj): selected crash type by user

    Returns:
        obj: selected geography, year and crash type info to be used in hidden datable

    """
    if not selected_crash_type:
        data = [
            {
                "Geography": selected_Geography,
                "Year": selected_Year,
                "Selected Crash Type": "All",
            }
        ]
    else:
        data = [
            {
                "Geography": selected_Geography,
                "Year": selected_Year,
                "Selected Crash Type": ast.literal_eval(
                    json.dumps(selected_crash_type, indent=2)
                    )
                .get("points")[0]
                .get("y"),
            }
        ]
    return data

# callback funtion to update daily table_2 (hour, number of crashes)
@app.callback(Output("DailyTable_2", "data"), [Input("DailyGraph", "figure")])
def udpate_daily_table_2(daily):
    """Update the daily table_2 hour and number of crashes info

    Args:
        daily (obj): daily crash graph

    Returns:
        obj: hour and number of crashes

    """
    data = []
    try:
        for i in range(24):
            data.append(
                {
                    "Hour": daily["data"][0]["x"][i],
                    "Daily Number of Crashes": daily["data"][0]["y"][i],
                }
            )
    except IndexError:
        pass
    return data

@app.callback(
    Output('daily_download', 'href'),
    [Input("DailyTable_2", "data")])
def update_download_link(data):
    download_data=pd.DataFrame(data)
    csv_string = download_data.to_csv(index=False, encoding='utf-8')
    csv_string = "data:text/csv;charset=utf-8," + urllib.parse.quote(csv_string)
    return csv_string

# callback funtion to update weather condition pie chart
@app.callback(
    Output("WeatherGraph", "figure"),
    [
        Input("Geography", "value"),
        Input("CrashTypeGraph", "clickData"),
        Input('Year', 'value'),
        Input('total_crash_click','n_clicks'),
        Input('fatality_click','n_clicks'),
        Input('injury_click','n_clicks'),
        Input('bike_click','n_clicks'),
        Input('ped_click','n_clicks'),
        Input('heavy_click','n_clicks'),
        Input('session-id', 'children')
    ],
    )
def update_weather_graph(
    selected_Geography,
    clickData,
    selected_Year,
    total_crash_click,
    fatality_click,
    injury_click,
    bike_click,
    ped_click,
    heavy_click,
    session_id
    ):
    """Update weather condition chart based on Geography and Year filter values and user selected individual main crash type

    Args:
        selected_Geography (str): Chamapign County, University District, municipalities, Urban (Within MPA boundary), Rural (Outside of MPA boundary)
        clickData: user selected individual crash type
        selected_Year (int): start_yr - end_yr
        session_id: user session
        fatality_click & stat_total_highlight, etc.: wheter the fatality or other statistics is selected

    Returns:
        figure: weather condition pie chart

    """
    df_filter = get_dataframe(selected_Geography, selected_Year, session_id)
    df_stat=filter_stat(df_filter)
    if not clickData:
        dfff=df_stat
    else:
        CrashTypeSelected=(
            ast.literal_eval(json.dumps(clickData, indent=2))
            .get("points")[0]
            .get('y')
            )
        dfff = filter_dataframe(df_stat, CrashTypeSelected)
    Weather=dfff['WeatherCondition'].value_counts()
    chart_labels = Weather.head(3).index.to_list()
    chart_labels.append("Other")
    chart_values = Weather.head(3).values.tolist()
    chart_values.append(sum(Weather.tail(-3)))
    data = [
        go.Pie(
            labels=chart_labels,
            values=chart_values,
            hole=0.5,
            marker={"colors": ["#1f77b4", "#aec7e8", "#c5b0d5", "#F2F022"]},
        )
    ]
    layout = go.Layout(
        legend=dict(x=1, y=1), margin=dict(l=10, r=10, t=20, b=20), height=180
    )
    figure = go.Figure(data=data, layout=layout)
    return figure

# callback funtion to update weather table_1 (municipality, year, selected crash type)
@app.callback(
    Output("WeatherTable_1", "data"),
    [
        Input("Geography", "value"),
        Input("Year", "value"),
        Input("CrashTypeGraph", "clickData"),
    ],
)
def udpate_weather_table_1(
    selected_Geography, selected_Year, selected_crash_type
):
    """Update the weather table_1 municipality, year, selected crash type info

    Args:
        selected_Geography (str): Chamapign County, University District, municipalities, Urban (Within MPA boundary), Rural (Outside of MPA boundary)
        selected_Year (int): start_yr - end_yr
        selected_crash_type (obj): selected crash type by user

    Returns:
        obj: selected geography, year and crash type info to be used in hidden datable

    """
    if not selected_crash_type:
        data = [
            {
                "Geography": selected_Geography,
                "Year": selected_Year,
                "Selected Crash Type": "All",
            }
        ]
    else:
        data = [
            {
                "Geography": selected_Geography,
                "Year": selected_Year,
                "Selected Crash Type": ast.literal_eval(
                    json.dumps(selected_crash_type, indent=2)
                )
                .get("points")[0]
                .get("y"),
            }
        ]
    return data

# callback funtion to update weather table_2 (weather condition, share of crashes)
@app.callback(
    Output("WeatherTable_2", "data"), [Input("WeatherGraph", "figure")]
)
def udpate_weather_table_2(weather):
    """Update the weather table _2 weather condition and share of crashes info

    Args:
        weather (obj): weather pie chart

    Returns:
        obj: weather condition and share of crashes info

    """
    data = []
    total = sum(weather["data"][0]["values"])
    try:
        for i in range(4):
            if total==0:
                data.append(
                    {
                        'Weather Condition':weather['data'][0]['labels'][i],
                        'Weather Share of Crashes': 0
                        }
                    )
            else:
                data.append(
                    {
                        "Weather Condition": weather["data"][0]["labels"][i],
                        "Weather Share of Crashes": (
                            weather["data"][0]["values"][i] / total
                        ),
                    }
                )
    except IndexError:
        pass
    return data

@app.callback(
    Output('weather_download', 'href'),
    [Input("WeatherTable_2", "data")])
def update_download_link(data):
    download_data=pd.DataFrame(data)
    csv_string = download_data.to_csv(index=False, encoding='utf-8')
    csv_string = "data:text/csv;charset=utf-8," + urllib.parse.quote(csv_string)
    return csv_string

# callback funtion to update Lighting condition pie chart
@app.callback(
    Output("LightingGraph", "figure"),
    [
        Input("Geography", "value"),
        Input("CrashTypeGraph", "clickData"),
        Input('Year', 'value'),
        Input('total_crash_click','n_clicks'),
        Input('fatality_click','n_clicks'),
        Input('injury_click','n_clicks'),
        Input('bike_click','n_clicks'),
        Input('ped_click','n_clicks'),
        Input('heavy_click','n_clicks'),
        Input('session-id', 'children')
    ],
    )
def update_lighting_graph(
    selected_Geography,
    clickData,
    selected_Year,
    total_crash_click,
    fatality_click,
    injury_click,
    bike_click,
    ped_click,
    heavy_click,
    session_id
    ):
    """Update light condition chart based on Geography and Year filter values and user selected individual main crash type

    Args:
        selected_Geography (str): Chamapign County, University District, municipalities, Urban (Within MPA boundary), Rural (Outside of MPA boundary)
        clickData: user selected individual crash type
        selected_Year (int): start_yr - end_yr
        session_id: user session
        fatality_click & stat_total_highlight, etc.: wheter the fatality or other statistics is selected

    Returns:
        figure: light condition pie chart

    """
    df_filter = get_dataframe(selected_Geography, selected_Year, session_id)
    df_stat=filter_stat(df_filter)
    if not clickData:
        dfff=df_stat
    else:
        CrashTypeSelected=(
            ast.literal_eval(
                json.dumps(clickData, indent=2)
                )
            .get("points")[0]
            .get('y')
            )
        dfff = filter_dataframe(df_stat, CrashTypeSelected)
    Lighting=dfff['LightCondition'].value_counts()
    chart_labels = Lighting.head(3).index.to_list()
    chart_labels.append("Other")
    chart_values = Lighting.head(3).values.tolist()
    chart_values.append(sum(Lighting.tail(-3)))
    data = [
        go.Pie(
            labels=chart_labels,
            values=chart_values,
            hole=0.5,
            marker={"colors": ["#1f77b4", "#aec7e8", "#c5b0d5", "#F2F022"]},
        )
    ]
    layout = go.Layout(
        legend=dict(x=1, y=1), margin=dict(l=10, r=10, t=20, b=20), height=180
    )
    figure = go.Figure(data=data, layout=layout)
    return figure

# callback funtion to update light table_1 (municipality, year, selected crash type)
@app.callback(
    Output("LightTable_1", "data"),
    [
        Input("Geography", "value"),
        Input("Year", "value"),
        Input("CrashTypeGraph", "clickData"),
    ],
)
def udpate_light_table_1(
    selected_Geography, selected_Year, selected_crash_type
):
    """Update the light table_1 municipality, year, selected crash type info

    Args:
        selected_Geography (str): Chamapign County, University District, municipalities, Urban (Within MPA boundary), Rural (Outside of MPA boundary)
        selected_Year (int): start_yr - end_yr
        selected_crash_type (obj): selected crash type by user

    Returns:
        obj: selected geography, year and crash type info to be used in hidden datable

    """
    if not selected_crash_type:
        data = [
            {
                "Geography": selected_Geography,
                "Year": selected_Year,
                "Selected Crash Type": "All",
            }
        ]
    else:
        data = [
            {
                "Geography": selected_Geography,
                "Year": selected_Year,
                "Selected Crash Type": ast.literal_eval(
                    json.dumps(selected_crash_type, indent=2)
                )
                .get("points")[0]
                .get("y"),
            }
        ]
    return data

# callback funtion to update light table_2 (light condition, share of crashes)
@app.callback(
    Output("LightTable_2", "data"), [Input("LightingGraph", "figure")]
)
def udpate_light_table_2(light):
    """Update the light table _2 light condition and share of crashes info

    Args:
        light (obj): light pie chart

    Returns:
        obj: light condition and share of crashes info

    """
    data = []
    total = sum(light["data"][0]["values"])
    try:
        for i in range(4):
            if total==0:
                data.append(
                    {
                        'Light Condition':light['data'][0]['labels'][i],
                        'Light Share of Crashes': 0
                        }
                    )
            else:
                data.append(
                    {
                        "Light Condition": light["data"][0]["labels"][i],
                        "Light Share of Crashes": (
                            light["data"][0]["values"][i] / total
                        ),
                    }
                )
    except IndexError:
        pass
    return data

@app.callback(
    Output('light_download', 'href'),
    [Input("LightTable_2", "data")])
def update_download_link(data):
    download_data=pd.DataFrame(data)
    csv_string = download_data.to_csv(index=False, encoding='utf-8')
    csv_string = "data:text/csv;charset=utf-8," + urllib.parse.quote(csv_string)
    return csv_string

# callback funtion to update road surface condition pie chart
@app.callback(
    Output("RoadSurfaceGraph", "figure"),
    [
        Input("Geography", "value"),
        Input("CrashTypeGraph", "clickData"),
        Input('Year', 'value'),
        Input('total_crash_click','n_clicks'),
        Input('fatality_click','n_clicks'),
        Input('injury_click','n_clicks'),
        Input('bike_click','n_clicks'),
        Input('ped_click','n_clicks'),
        Input('heavy_click','n_clicks'),
        Input('session-id', 'children')
    ],
    )
def update_road_surface_graph(
    selected_Geography,
    clickData,
    selected_Year,
    total_crash_click,
    fatality_click,
    injury_click,
    bike_click,
    ped_click,
    heavy_click,
    session_id
    ):
    """Update road surface condition pie chart based on Geography and Year filter values and user selected individual main crash type

    Args:
        selected_Geography (str): Chamapign County, University District, municipalities, Urban (Within MPA boundary), Rural (Outside of MPA boundary)
        clickData: user selected individual crash type
        selected_Year (int): 2014-201
        session_id: user session
        fatality_click & stat_total_highlight, etc.: wheter the fatality or other statistics is selected

    Returns:
        figure: road surface condition pie chart

    """
    df_filter = get_dataframe(selected_Geography, selected_Year, session_id)
    df_stat=filter_stat(df_filter)
    if not clickData:
        dfff=df_stat
    else:
        CrashTypeSelected=(
            ast.literal_eval(
                json.dumps(clickData, indent=2)
                )
            .get("points")[0]
            .get('y')
            )
        dfff = filter_dataframe(df_stat, CrashTypeSelected)
    RoadSurface=dfff['RoadSurfaceCond'].value_counts()
    chart_labels = RoadSurface.head(3).index.to_list()
    chart_labels.append("Other")
    chart_values = RoadSurface.head(3).values.tolist()
    chart_values.append(sum(RoadSurface.tail(-3)))
    data = [
        go.Pie(
            labels=chart_labels,
            values=chart_values,
            hole=0.5,
            marker={"colors": ["#1f77b4", "#aec7e8", "#c5b0d5", "#F2F022"]},
        )
    ]
    layout = go.Layout(
        legend=dict(x=1, y=1),
        # legend_orientation="h",
        margin=dict(l=10, r=10, t=20, b=20),
        height=180,
    )
    figure = go.Figure(data=data, layout=layout)
    return figure

# callback funtion to update surface table_1 (municipality, year, selected crash type)
@app.callback(
    Output("SurfaceTable_1", "data"),
    [
        Input("Geography", "value"),
        Input("Year", "value"),
        Input("CrashTypeGraph", "clickData"),
    ],
)
def udpate_surface_table_1(
    selected_Geography, selected_Year, selected_crash_type
):
    """Update the surface table_1 municipality, year, selected crash type info

    Args:
        selected_Geography (str): Chamapign County, University District, municipalities, Urban (Within MPA boundary), Rural (Outside of MPA boundary)
        selected_Year (int): start_yr - end_yr
        selected_crash_type (obj): selected crash type by user

    Returns:
        obj: selected geography, year and crash type info to be used in hidden datable

    """
    if not selected_crash_type:
        data = [
            {
                "Geography": selected_Geography,
                "Year": selected_Year,
                "Selected Crash Type": "All",
            }
        ]
    else:
        data = [
            {
                "Geography": selected_Geography,
                "Year": selected_Year,
                "Selected Crash Type": ast.literal_eval(
                    json.dumps(selected_crash_type, indent=2)
                )
                .get("points")[0]
                .get("y"),
            }
        ]
    return data

# callback funtion to update surface table_2 (road surface condition, share of crashes)
@app.callback(
    Output("SurfaceTable_2", "data"), [Input("RoadSurfaceGraph", "figure")]
)
def udpate_surface_table_2(surface):
    """Update the surface table _2 road surface condition and share of crashes info

    Args:
        surface (obj): road surface condition pie chart

    Returns:
        obj: road surface condition and share of crashes info

    """
    data = []
    total = sum(surface["data"][0]["values"])
    try:
        for i in range(4):
            if total==0:
                data.append(
                    {
                        'Surface Condition':surface['data'][0]['labels'][i],
                        'Surface Share of Crashes': 0
                     }
                    )
            else:
                data.append(
                    {
                        "Surface Condition": surface["data"][0]["labels"][i],
                        "Surface Share of Crashes": (
                            surface["data"][0]["values"][i] / total
                        ),
                    }
                )
    except IndexError:
        pass
    return data

@app.callback(
    Output('surface_download', 'href'),
    [Input("SurfaceTable_2", "data")])
def update_download_link(data):
    download_data=pd.DataFrame(data)
    csv_string = download_data.to_csv(index=False, encoding='utf-8')
    csv_string = "data:text/csv;charset=utf-8," + urllib.parse.quote(csv_string)
    return csv_string

# callback funtion to update crash location map
@app.callback(
    Output("map-graph", "figure"),
    [
        Input("Geography", "value"),
        Input("CrashTypeGraph", "clickData"),
        Input('Year', 'value'),
        Input('total_crash_click','n_clicks'),
        Input('fatality_click','n_clicks'),
        Input('injury_click','n_clicks'),
        Input('bike_click','n_clicks'),
        Input('ped_click','n_clicks'),
        Input('heavy_click','n_clicks'),
        Input('session-id', 'children')
    ],
    )
def update_map(
    selected_Geography,
    clickData,
    selected_Year,
    total_crash_click,
    fatality_click,
    injury_click,
    bike_click,
    ped_click,
    heavy_click,
    session_id
    ):
    """Update crash location map based on Geography and Year filter values and user selected individual main crash type

    Args:
        selected_Geography (str): Chamapign County, University District, municipalities, Urban (Within MPA boundary), Rural (Outside of MPA boundary)
        clickData: user selected individual crash type
        selected_Year (int): start_yr - end_yr
        session_id: user session
        fatality_click & stat_total_highlight, etc.: wheter the fatality or other statistics is selected

    Returns:
        figure: crash location map

    """
    df_filter = get_dataframe(selected_Geography, selected_Year, session_id)
    df_stat=filter_stat(df_filter)
    Points=df_filter
    if not clickData:
        Points=df_stat
    else:
        CrashTypeSelected = (
            ast.literal_eval(json.dumps(clickData, indent=2))
            .get("points")[0]
            .get("y")
        )
        Points = filter_dataframe(df_stat, CrashTypeSelected)
    try:
        return gen_map(Points)
    except:
        return gen_map(df_filter)

debug = os.getenv("DEBUG", "True")
if debug == "False":
    server = app.server
elif __name__ == "__main__":
    app.run_server(host="0.0.0.0")
