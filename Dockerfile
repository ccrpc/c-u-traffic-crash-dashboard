FROM python:3.8-slim
RUN mkdir /app
WORKDIR /app
COPY requirements.txt /app/
RUN apt-get update && apt-get install -y libgeos-dev \
    && pip install -U pip \
    && pip install --no-cache-dir -r requirements.txt \
    && rm -rf /var/lib/apt/lists/*
ENV PYTHONUNBUFFERED 1
ENV DEBUG False
ENV MAPBOX_KEY NO_TOKEN
ENV CSV_FILE_RISK_PREDICTION /app/data/dashboard_risk_prediction.csv
ENV URL_BASE_PATHNAME /
COPY crash_dash.py /app/
COPY assets /app/assets/
VOLUME [ "/app/data" ]

EXPOSE 8050
CMD ["gunicorn", "-b", ":8050", "crash_dash:server"]
