# Changelog

All notable changes to this project will be documented in this file. See [commit-and-tag-version](https://github.com/absolute-version/commit-and-tag-version) for commit guidelines.

## [0.1.6](https://gitlab.com/ccrpc/c-u-traffic-crash-dashboard/compare/v0.1.5...v0.1.6) (2024-09-05)


### Features

* Converting Skaffold to use Kind ([14eb0c6](https://gitlab.com/ccrpc/c-u-traffic-crash-dashboard/commit/14eb0c6c001fdc7ac18f814f68e9a8fb82ced553))
* Loading 2023 data ([695f4fa](https://gitlab.com/ccrpc/c-u-traffic-crash-dashboard/commit/695f4faadb48ba1cc0dbc7c3323e2bc59738f260))


### Bug Fixes

* Broken link due to Revize Move ([cc59798](https://gitlab.com/ccrpc/c-u-traffic-crash-dashboard/commit/cc59798f0fc4d80c3edb2237358a93107a10aa4c))
* Not pulling file name from env variable ([173e911](https://gitlab.com/ccrpc/c-u-traffic-crash-dashboard/commit/173e911af0a9fc30ad32f96af9cc9d2cd486c220))
* Updating Logo ([56e3caa](https://gitlab.com/ccrpc/c-u-traffic-crash-dashboard/commit/56e3caae7e986b8b20e25264d95778e18dcff7ba))

## [0.1.5](https://gitlab.com/ccrpc/c-u-traffic-crash-dashboard/compare/v0.1.4...v0.1.5) (2023-10-04)


### Features

* university and 2022 data ([577a8dd](https://gitlab.com/ccrpc/c-u-traffic-crash-dashboard/commit/577a8dd89525b293153baa63656a56ea77416518))
* update crash data to 2017-2021 ([3427588](https://gitlab.com/ccrpc/c-u-traffic-crash-dashboard/commit/3427588b7fefc00e2a7940ceba2fe1793edd406a))


### Bug Fixes

* **data:** Updating a few more things to the 2017 to 2021 ([5ceb9fa](https://gitlab.com/ccrpc/c-u-traffic-crash-dashboard/commit/5ceb9fa28b56bd0b6f902e7170aa02f74e533659))
* fix download errors ([ad78f02](https://gitlab.com/ccrpc/c-u-traffic-crash-dashboard/commit/ad78f022752f71e972b1af34929f198178fc3ea4))
* Fixed highlight throwing error ([1a048f1](https://gitlab.com/ccrpc/c-u-traffic-crash-dashboard/commit/1a048f1619576e61a6d915b484bd1246d36deb13))
* Hover value broken if crash cause is null ([17542e2](https://gitlab.com/ccrpc/c-u-traffic-crash-dashboard/commit/17542e2f76428107353e3369bc86341cc18fea02))
* Not pulling file name from env variable ([4033b24](https://gitlab.com/ccrpc/c-u-traffic-crash-dashboard/commit/4033b24b1a083c0a6fb943eab5d76b03b042fff6))

### [0.1.4](https://gitlab.com/ccrpc/c-u-traffic-crash-dashboard/compare/v0.1.3...v0.1.4) (2022-10-26)


### Bug Fixes

* **data:** Updating a few more things to the 2017 to 2021 ([9f2926e](https://gitlab.com/ccrpc/c-u-traffic-crash-dashboard/commit/9f2926ef4f366202a30e44b275bc9593a6410876))

### [0.1.3](https://gitlab.com/ccrpc/c-u-traffic-crash-dashboard/compare/v0.1.2...v0.1.3) (2021-10-13)

### [0.1.2](https://gitlab.com/ccrpc/c-u-traffic-crash-dashboard/compare/v0.1.1...v0.1.2) (2021-09-17)

### Bug Fixes

-   create separate env variable for prediction DF ([5d8725c](https://gitlab.com/ccrpc/c-u-traffic-crash-dashboard/commit/5d8725c63d741027ed274c297824bb4efe11a34a))

### [0.1.1](https://gitlab.com/ccrpc/c-u-traffic-crash-dashboard/compare/v0.1.0...v0.1.1) (2021-09-17)

## 0.1.0 (2021-09-17)

### Features

-   add environment variable for deployment path ([6bcb7c1](https://gitlab.com/ccrpc/c-u-traffic-crash-dashboard/commit/6bcb7c13f96c03806d31cbc9349ac76ec8043e70))

### Bug Fixes

-   remove hard-coded URLs ([cfb92be](https://gitlab.com/ccrpc/c-u-traffic-crash-dashboard/commit/cfb92bef55f95bd80125caceabc3efb73d1528db))
